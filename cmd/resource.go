/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
  "fmt"
  "os"

  "github.com/spf13/cobra"
  "k8s.io/apimachinery/pkg/runtime/schema"
  "k8s.io/client-go/discovery"
  "k8s.io/client-go/discovery/cached/memory"
  "k8s.io/client-go/restmapper"
  "k8s.io/client-go/tools/clientcmd"
)

// resourceCmd represents the resource command
var resourceCmd = &cobra.Command{
  Use:   "resource",
  Short: "A brief description of your command",
  Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
  Args: cobra.ExactArgs(1),
  Run: func(cmd *cobra.Command, args []string) {
    r := args[0]
    gvr, err := RetrieveInfo(r)
    if err != nil {
      fmt.Printf("Resource not found! - error: %s\n", err.Error())
      os.Exit(1)
    }

    fmt.Printf("Group: %s, Version: %s, Resource: %s\n", gvr.Group, gvr.Version, gvr.Resource)
  },
}

func init() {
  explainCmd.AddCommand(resourceCmd)

  // Here you will define your flags and configuration settings.

  // Cobra supports Persistent Flags which will work for this command
  // and all subcommands, e.g.:
  // resourceCmd.PersistentFlags().String("foo", "", "A help for foo")

  // Cobra supports local flags which will only run when this command
  // is called directly, e.g.:
  // resourceCmd.Flags().StringP("toggle", "t", false, "Help message for toggle")

}

func RetrieveInfo(r string) (schema.GroupVersionResource, error) {
  gvr := schema.GroupVersionResource{
    Resource: r,
  }

  config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
  if err != nil {
    return schema.GroupVersionResource{}, err
  }
  dc, err := discovery.NewDiscoveryClientForConfig(config)

  if err != nil {
    return schema.GroupVersionResource{}, err
  }

  restmapper := restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(dc))

  gvr, err = restmapper.ResourceFor(gvr)
  if err != nil {
    return schema.GroupVersionResource{}, err
  }
  return gvr, nil
}
